"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PercentDataWithControls = void 0;
const utils_1 = require("./utils");
/**
 * A class for datasets where the sample size varies (P-Charts)
 */
class PercentDataWithControls {
    /**
     *
     * @constructor Constructor method for PercentDataWithControls
     * @param {number[]} defects - A list of the defect occurences (numerator)
     * @param {number[]} populationSizes - A list of the population sizes (denominator)
     */
    constructor(defects, sampleSizes) {
        this.defects = defects;
        this.populationSizes = sampleSizes;
        this.p = utils_1.precN(utils_1.sum(defects) / utils_1.sum(sampleSizes), 10);
    }
    calcPControls(p) {
        return (n) => 3 * Math.sqrt((p * (1 - p)) / n);
    }
    getMeanArr() {
        return utils_1.repeatElement(this.p * 100, this.defects.length);
    }
    getLowerBoundsArr() {
        const controls = this.calcPControls(this.p);
        return this.populationSizes.map((sampleSize) => utils_1.precN(this.p - controls(sampleSize), 10) * 100);
    }
    getUpperBoundsArr() {
        const controls = this.calcPControls(this.p);
        return this.populationSizes.map((sampleSize) => utils_1.precN(this.p + controls(sampleSize), 10) * 100);
    }
    getData() {
        return {
            defects: this.defects,
            populationSizes: this.populationSizes,
            percent: this.defects.map((defect, i) => utils_1.precN(defect / this.populationSizes[i], 10) * 100),
            mean: this.getMeanArr(),
            upperBounds: this.getUpperBoundsArr(),
            lowerBounds: this.getLowerBoundsArr(),
        };
    }
}
exports.PercentDataWithControls = PercentDataWithControls;
//# sourceMappingURL=PercentDataWithControls.js.map