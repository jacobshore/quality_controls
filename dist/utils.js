"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.repeatElement = exports.precN = exports.sum = void 0;
function sum(arr) {
    return arr.reduce((a, b) => a + b);
}
exports.sum = sum;
function precN(flt, prec) {
    return parseFloat(flt.toPrecision(prec));
}
exports.precN = precN;
function repeatElement(el, times) {
    const arr = [];
    for (let i = 0; i < times; i++) {
        arr.push(el);
    }
    return arr;
}
exports.repeatElement = repeatElement;
//# sourceMappingURL=utils.js.map