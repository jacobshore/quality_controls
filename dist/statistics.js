"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calcStDev = exports.calcStDevSamp = exports.calcStDevPop = exports.calcMean = exports.DataSetType = void 0;
var DataSetType;
(function (DataSetType) {
    DataSetType["Population"] = "population";
    DataSetType["Sample"] = "sample";
})(DataSetType = exports.DataSetType || (exports.DataSetType = {}));
const calcMean = (arr) => arr.length !== 0 ? arr.reduce((a, b) => a + b) / arr.length : 0.0;
exports.calcMean = calcMean;
const calcStDevPop = (pop) => {
    if (pop.length === 0)
        return 0.0;
    const mean = exports.calcMean(pop);
    return Math.sqrt(pop.map((x) => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / pop.length);
};
exports.calcStDevPop = calcStDevPop;
const calcStDevSamp = (sample) => Math.sqrt(sample
    .map((x) => x - sample.reduce((a, b) => a + b) / sample.length)
    .map((x) => Math.pow(x, 2))
    .reduce((a, b) => a + b) /
    (sample.length - 1));
exports.calcStDevSamp = calcStDevSamp;
const calcStDev = (arr, dataSetType) => dataSetType === DataSetType.Population
    ? exports.calcStDevPop(arr)
    : exports.calcStDevSamp(arr);
exports.calcStDev = calcStDev;
//# sourceMappingURL=statistics.js.map