import { DataWithControls } from "../src/DataWithControls";
/* eslint-disable no-undef */

const popData = new DataWithControls([
  871.4877029394012,
  853.1730065487205,
  844.3521933824229,
  835.5920920789159,
  825.6210839148167,
  817.738764037498,
  817.4106137228473,
  805.957454686073,
  799.8878965473625,
  789.2238315846375,
  787.4245615701781,
  785.841455449,
  744.60363221284,
  726.6576558883895,
  718.3542426493096,
  700.0463477809434,
  685.2045266603985,
  674.7892363765275,
]);
const sampleData = new DataWithControls(
  [
    871.4877029394012,
    853.1730065487205,
    844.3521933824229,
    835.5920920789159,
    825.6210839148167,
    817.738764037498,
    817.4106137228473,
    805.957454686073,
    799.8878965473625,
    789.2238315846375,
    787.4245615701781,
    785.841455449,
    744.60363221284,
    726.6576558883895,
    718.3542426493096,
    700.0463477809434,
    685.2045266603985,
    674.7892363765275,
  ],
  1
);

describe("DataWithControls for Population", () => {
  const populationData = popData.getData();
  it("getData returns defects, mean, upperBounds, and lowerBounds", () => {
    expect(populationData.hasOwnProperty("defects")).toBe(true);
    expect(populationData.hasOwnProperty("mean")).toBe(true);
    expect(populationData.hasOwnProperty("upperBounds")).toBe(true);
    expect(populationData.hasOwnProperty("lowerBounds")).toBe(true);
  });
  it("Values in mean, lowerBounds, and upperBounds are always the same", () => {
    const { defects, mean, lowerBounds, upperBounds } = populationData;
    expect(new Set(mean).size).toEqual(1);
    expect(new Set(upperBounds).size).toEqual(1);
    expect(new Set(lowerBounds).size).toEqual(1);
    expect(new Set(defects).size).toBeGreaterThan(1);
  });
  it("Mean is the correct mean", () => {
    const { mean } = populationData;
    expect(mean[0]).toEqual(782.4092387794601);
  });
  it("upperBounds are the correct upperBounds for population", () => {
    const { upperBounds } = populationData;
    expect(upperBounds[0]).toEqual(957.55);
  });
  it("lowerBounds are the correct upperBounds for population", () => {
    const { lowerBounds } = populationData;
    expect(lowerBounds[0]).toEqual(607.27);
  });
});

describe("DataWithControls for Sample", () => {
  const sd = sampleData.getData();
  it("upperBounds are correct for sample data", () => {
    const { upperBounds } = sd;
    expect(upperBounds[0]).toEqual(962.62);
  });
  it("lowerBounds are correct for sample data", () => {
    const { lowerBounds } = sd;
    expect(lowerBounds[0]).toEqual(602.19);
  });
});
