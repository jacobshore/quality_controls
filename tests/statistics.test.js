/* eslint-disable no-undef */
import {
  calcMean,
  calcStDev,
  calcStDevPop,
  calcStDevSamp,
} from "../src/statistics";

const arr1 = [
  6, 11, 11, 10, 8, 10, 10, 6, 8, 6, 8, 11, 10, 5, 6, 5, 11, 4, 10, 4, 8,
];
const arr2 = [
  14, 74, 48, 35, 55, 61, 54, 53, 0, 45, 70, 51, 42, 62, 62, 77, 65, 42, 62, 27,
  36,
];

describe("Calculate mean", () => {
  it("Should return the mean of the array", () => {
    expect(calcMean([0, 10])).toEqual(5);
  });
  it("Should return zero for an empty array", () => {
    expect(calcMean([])).toEqual(0.0);
  });
  it("Should return 0 when the sum of the array is 0", () => {
    expect(calcMean([0, 0, 0])).toEqual(0);
  });
});

describe("Calculate standard deviation of population", () => {
  it("Should return the standard deviation of an array", () => {
    expect(calcStDevPop(arr1).toFixed(5)).toEqual((2.4102953780655).toFixed(5));
    expect(calcStDevPop(arr2).toFixed(5)).toEqual((18.805123053605).toFixed(5));
  });
  it("Should return 0 for an empty array", () => {
    expect(calcStDevPop([])).toEqual(0);
  });
  it("Should return 0 for an array with a sum of 0", () => {
    expect(calcStDevPop([0, 0, 0, 0])).toEqual(0);
  });
});

describe("calculate standard deviation of sample", () => {
  expect(calcStDevSamp(arr1).toFixed(5)).toEqual((2.4698178070457).toFixed(5));
  expect(calcStDevSamp(arr2).toFixed(5)).toEqual((19.26951700781).toFixed(5));
  expect(calcStDevSamp([2, 2, 5, 7]).toFixed(5)).toEqual(
    (2.4494897427832).toFixed(5)
  );
});

describe("Calculate standard deviation based on whether it's a sample or a population", () => {
  expect(calcStDev(arr1, "population").toFixed(5)).toEqual(
    (2.4102953780655).toFixed(5)
  );
  expect(calcStDev(arr2, "population").toFixed(5)).toEqual(
    (18.805123053605).toFixed(5)
  );
  expect(calcStDev(arr1, "sample").toFixed(5)).toEqual(
    (2.4698178070457).toFixed(5)
  );
  expect(calcStDev(arr2, "sample").toFixed(5)).toEqual(
    (19.26951700781).toFixed(5)
  );
  expect(calcStDev([2, 2, 5, 7], "sample").toFixed(5)).toEqual(
    (2.4494897427832).toFixed(5)
  );
});
